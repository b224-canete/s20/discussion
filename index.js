// console.log("Hello World!");

// REPEATITION CONTROL STRUCTURES

/*
	While Loop

		-in while loop if the condition is true, it executes the code/statement until the condition is no longer true

		Syntax:
			while (expression/condition) {
				statement
			};

*/

let count = 5;

	while(count !== 0) {
		console.log("While: " + count);
		count--; //count -= 1
	};

/*
	Mini Activity: (3 minutes)
		- Create a while loop that will display numbers from 1 to 5 only.
		- Debug the following code:
*/	

let x = 1
	
	while(x <=5 ) {
		console.log(x);
		x++;
	};


/*
	Do While Loop
		- A do-while loop works a lot like the while loop. But unlike whhile loop, do-while loop guarantees the code will be executed at least once.

		Syntax:
			do {
				statement;
			} while (expression/condition);
*/

// let number = Number(prompt("Give me a number: "));

// 	do {
// 		console.log("Do while: " + number);
// 		number += 1;
// 	} while (number < 10);


/*
	FOR LOOP
		- more flexible than while loop and do-while loop

		Syntax:

			for ( initialization; expression/condition; finalExpression) {
				statement;
			}	
*/

		for(let count = 1; count <= 20; count+=2) {
			console.log("For Loop: " + count);
		}


// The ".length" property

// Characters in strings may be counted using the .length property.
// Strings are special ciompared to other data types in a way that it has access to functions and other piece of information another primitive data might not have.

let myString = "Iron Man";
console.log(myString.length);

// Accessing characters of a string

let s=3;
console.log(myString[0]);
console.log(myString[s]);	
console.log(myString[myString.length-1]);

for (let x =0; x < myString.length; x++) {
	console.log(myString[x]);
};

console.log(" ")
let myName = "FERNANDO"; //8 characters

for(let i = 0; i < myName.length ; i++){
	if(
		myName[i].toLowerCase() == "a" || 
		myName[i].toLowerCase() == "e" || 
		myName[i].toLowerCase() == "i" || 
		myName[i].toLowerCase() == "o" || 
		myName[i].toLowerCase() == "u"  
	) {
		console.log("Vowel");
	} else {
		console.log(myName[i]);
	};
}

// Mini Activity

/*
	- create a varaible that will contain the string "extravagant"
	- create another varaible that will store the consonants from the string.
	- create a for loop that will iterate through the individual letters of the string based on it's length.
	- create an if statement that will check if the individual letters of the string is equal to a vowel and continue to the next iteration of the loop if it is true.
	- create an else statement that will add the consonants to the second
*/

let origWord = "extravagant";
let consoWord = "";

for(let y = 0; y < origWord.length; y++) {
	if (
		origWord[y].toLowerCase() == "a" ||
		origWord[y].toLowerCase() == "e" ||
		origWord[y].toLowerCase() == "i" ||
		origWord[y].toLowerCase() == "o" ||
		origWord[y].toLowerCase() == "u" 
	) {
		continue;
	} else {
		consoWord += origWord [y];
	};
}

console.log(consoWord);


// Continue and Break Statements

/*
	"continue" statement allows the code to go to the next iteration of the loop without finishing the execution of all the statements in a code block.

	"break" statement is used to terminate the current loop once a match has been found.
*/

for(let count = 0; count <= 20; count++) {

	console.log("Hello World: " + count);

	if(count % 2 === 0){
		console.log("Even Number");
		continue;
	};

	if (count > 10) {
		break;
	};
};

let name = "Alejandro";

for(let i = 0; i<name.length; i++) {
	console.log(name[i]);

	if(name[i].toLowerCase()=== "a"){
		console.log("Continue to the next iteration");
		continue;
	};

	if (name[i].toLowerCase() === "d") {
		break;
	}
}